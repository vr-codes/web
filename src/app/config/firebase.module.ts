import {NgModule} from '@angular/core';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';

@NgModule({
  declarations: [],
  imports: [
    AngularFireModule.initializeApp({
      apiKey: environment.firebaseKey,
      authDomain: 'gallery-app-1.firebaseapp.com',
      databaseURL: 'https://gallery-app-1.firebaseio.com',
      storageBucket: 'gallery-app-1.appspot.com',
      projectId: 'gallery-app-1',
    }),
    AngularFireAuthModule,
    AngularFirestoreModule,
  ],
  exports: [
    AngularFireModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
  ]
})
export class FirebaseModule {
}
