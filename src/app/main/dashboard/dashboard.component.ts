import {Component, OnInit} from '@angular/core';
import {GalleryService} from '../../core/service/gallery.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Link} from '../../core/model/link.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  input: FormGroup = this.formBuilder.group({
    url: ['', [Validators.required]]
  });

  items: Link[];

  constructor(private galleryService: GalleryService, private formBuilder: FormBuilder) {
  }

  add() {
    this.galleryService.add(this.input.value);
  }

  ngOnInit(): void {
    this.galleryService.items.subscribe(value => {
      this.input.reset();
      this.items = value;
    });
  }

  remove(url: Link) {
    this.galleryService.remove(url);
  }

}
