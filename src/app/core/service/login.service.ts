import {Injectable} from '@angular/core';
import {LoginModel} from '../model/login.model';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable()
export class LoginService {

  constructor(private fireAuth: AngularFireAuth) {
  }

  public login(login: LoginModel) {
    return this.fireAuth.auth.signInWithEmailAndPassword(login.username, login.password);
  }

  public register(register: LoginModel) {
    return this.fireAuth.auth.createUserWithEmailAndPassword(register.username, register.password);
  }

  logout() {
    return this.fireAuth.auth.signOut();
  }
}
