import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Subject} from 'rxjs';
import {Link} from '../model/link.model';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable()
export class GalleryService {


  private itemStore: AngularFirestoreCollection<Link>;
  public items: Subject<Link[]> = new Subject<Link[]>();

  constructor(private fireDb: AngularFirestore, private fireAuth: AngularFireAuth) {
    this.fireAuth.user.subscribe(user => {
      this.itemStore = this.fireDb.collection<Link>('links', ref => ref.where('owner', '==', user.uid));
      this.itemStore.valueChanges().subscribe(value => this.items.next(value));
    });
  }

  add(link: Link) {
    this.fireAuth.user.subscribe(user => {
      link.owner = user.uid;
      this.itemStore.add(link);
    });
  }

  remove(url: Link) {

  }
}
