import {NgModule} from '@angular/core';
import {LoginService} from './service/login.service';
import {GalleryService} from './service/gallery.service';

@NgModule({
  providers: [LoginService, GalleryService]
})
export class CoreModule {
}
