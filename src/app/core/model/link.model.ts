export interface Link {
  owner: string;
  url: string;
}
