import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-error',
  template: '<span class="error">{{errorMessage}}</span>',
  styles: ['.error {color: red}']
})
export class ErrorComponent {

  static defaulMessages = {
    'required': 'Required',
    'email': 'Invalid e-mail address',
  };

  @Input()
  control: FormControl;

  @Input()
  customMessages: Map<string, string>;

  constructor() {
  }

  get errorMessage() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return this.getValidatorErrorMessage(propertyName);
      }
    }

    return null;
  }

  private getValidatorErrorMessage(validatorName: string) {
    const message = (this.customMessages || {})[validatorName];
    return message || ErrorComponent.defaulMessages[validatorName];
  }

}
