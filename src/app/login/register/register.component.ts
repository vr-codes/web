import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../core/service/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  registerForm: FormGroup = this.formBuilder.group({
    username: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  });

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private router: Router) {
  }

  create() {
    if (this.registerForm.valid) {
      this.loginService.register(this.registerForm.value).then(value => {
        this.router.navigateByUrl('/login');
      });
    }
  }
}
